package model;

/**
 * Created by richg on 2018-03-17.
 */

public class Calculator {

    public double add(double x, double y) {
        return x + y;
    }

    public double add(double x, double y, double z) {
        return x + y + z;
    }

    public double subtraction(double x, double y, double z) {return x - y - z; }

    public double multiplication(double x, double y, double z) {
        return x * y * z;
    }

    public double division(double x, double y, double z) {
        return x / y / z;
    }

    public double modulo(double x, double y, double z) {
        return x % y % z;
    }
}
