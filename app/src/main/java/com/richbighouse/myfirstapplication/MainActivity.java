package com.richbighouse.myfirstapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.concurrent.ScheduledThreadPoolExecutor;

import model.Calculator;

public class MainActivity extends AppCompatActivity {

    private final Calculator calculator = new Calculator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addBtn = findViewById(R.id.btnAdd);
        Button subtractionBtn = findViewById(R.id.btnSubstraction);
        Button multiplicationBtn = findViewById(R.id.btnMultiplication);
        Button divisionBtn = findViewById(R.id.btnDivision);
        Button moduloBtn = findViewById(R.id.btnModulo);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText firstNumberPlainText = findViewById(R.id.firstNumberPlainText);
                EditText secondNumberPlainText = findViewById(R.id.secondNumberPlainText);
                EditText thirdNumberPlainText = findViewById(R.id.thirdNumberPlainText);
                TextView resultTextView = (TextView) findViewById(R.id.resultTextView);

                String firstNumberString = firstNumberPlainText.getText().toString();
                String secondNumberString = secondNumberPlainText.getText().toString();
                String thirdNumberString = thirdNumberPlainText.getText().toString();

                double firstNumber = firstNumberPlainText.length() > 0 ? Double.parseDouble(firstNumberString) : 0;
                double secondNumber = secondNumberString.length() > 0 ? Double.parseDouble(secondNumberString) : 0;
                double thirdNumber = thirdNumberString.length() > 0 ? Double.parseDouble(thirdNumberString) : 0;

                double result = calculator.add(firstNumber, secondNumber, thirdNumber);

                resultTextView.setText(result+"");
            }
        });

        subtractionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText firstNumberPlainText = findViewById(R.id.firstNumberPlainText);
                EditText secondNumberPlainText = findViewById(R.id.secondNumberPlainText);
                EditText thirdNumberPlainText = findViewById(R.id.thirdNumberPlainText);
                TextView resultTextView = (TextView) findViewById(R.id.resultTextView);

                String firstNumberString = firstNumberPlainText.getText().toString();
                String secondNumberString = secondNumberPlainText.getText().toString();
                String thirdNumberString = thirdNumberPlainText.getText().toString();

                double firstNumber = firstNumberPlainText.length() > 0 ? Double.parseDouble(firstNumberString) : 0;
                double secondNumber = secondNumberString.length() > 0 ? Double.parseDouble(secondNumberString) : 0;
                double thirdNumber = thirdNumberString.length() > 0 ? Double.parseDouble(thirdNumberString) : 0;

                double result = calculator.subtraction(firstNumber, secondNumber, thirdNumber);

                resultTextView.setText(result+"");
            }
        });

        multiplicationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText firstNumberPlainText = findViewById(R.id.firstNumberPlainText);
                EditText secondNumberPlainText = findViewById(R.id.secondNumberPlainText);
                EditText thirdNumberPlainText = findViewById(R.id.thirdNumberPlainText);
                TextView resultTextView = (TextView) findViewById(R.id.resultTextView);

                String firstNumberString = firstNumberPlainText.getText().toString();
                String secondNumberString = secondNumberPlainText.getText().toString();
                String thirdNumberString = thirdNumberPlainText.getText().toString();

                double firstNumber = firstNumberPlainText.length() > 0 ? Double.parseDouble(firstNumberString) : 1;
                double secondNumber = secondNumberString.length() > 0 ? Double.parseDouble(secondNumberString) : 1;
                double thirdNumber = thirdNumberString.length() > 0 ? Double.parseDouble(thirdNumberString) : 1;

                double result = calculator.multiplication(firstNumber, secondNumber, thirdNumber);

                resultTextView.setText(result+"");
            }
        });

        divisionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText firstNumberPlainText = findViewById(R.id.firstNumberPlainText);
                EditText secondNumberPlainText = findViewById(R.id.secondNumberPlainText);
                EditText thirdNumberPlainText = findViewById(R.id.thirdNumberPlainText);
                TextView resultTextView = (TextView) findViewById(R.id.resultTextView);

                String firstNumberString = firstNumberPlainText.getText().toString();
                String secondNumberString = secondNumberPlainText.getText().toString();
                String thirdNumberString = thirdNumberPlainText.getText().toString();

                double firstNumber = firstNumberPlainText.length() > 0 ? Double.parseDouble(firstNumberString) : 1;
                double secondNumber = secondNumberString.length() > 0 ? Double.parseDouble(secondNumberString) : 1;
                double thirdNumber = thirdNumberString.length() > 0 ? Double.parseDouble(thirdNumberString) : 1;

                if(firstNumber != 0.0 && thirdNumber != 0.0 && thirdNumber != 0.0) {
                    double result = calculator.division(firstNumber, secondNumber, thirdNumber);
                    resultTextView.setText(result+"");
                } else {
                    resultTextView.setText("Error! Division by 0!");
                }
            }
        });

        moduloBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText firstNumberPlainText = findViewById(R.id.firstNumberPlainText);
                EditText secondNumberPlainText = findViewById(R.id.secondNumberPlainText);
                EditText thirdNumberPlainText = findViewById(R.id.thirdNumberPlainText);
                TextView resultTextView = (TextView) findViewById(R.id.resultTextView);

                String firstNumberString = firstNumberPlainText.getText().toString();
                String secondNumberString = secondNumberPlainText.getText().toString();
                String thirdNumberString = thirdNumberPlainText.getText().toString();

                double firstNumber = firstNumberPlainText.length() > 0 ? Double.parseDouble(firstNumberString) : 1;
                double secondNumber = secondNumberString.length() > 0 ? Double.parseDouble(secondNumberString) : 1;
                double thirdNumber = thirdNumberString.length() > 0 ? Double.parseDouble(thirdNumberString) : 1;

                double result = calculator.modulo(firstNumber, secondNumber, thirdNumber);

                resultTextView.setText(result+"");
            }
        });



    }
}
